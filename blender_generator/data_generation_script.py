import bpy
from math import radians
import random
import pickle
import os

import numpy as np

BASE_PATH = "/home/emadrigal/Documents/water_splash_dataset/test_with_effector_leaf/"

SIMULATION_START = 0
SIMULATION_END = 250

DENSITY_THRESHOLD = 0.1

WATER_INDEX = 1
SMOKE_INDEX = 2
LEAF_INDEX = 3

MAX_DROPLETS = 5
NUM_RENDERS = 10

RENDER = True

# Requirements The following elements should already exist:
# Camera
# Light
# Leaf


def get_camera_matrices(
    camera: bpy.types.Object,
    render: bpy.types.RenderSettings = bpy.context.scene.render,
):
    """
    Given a camera returns its projection and modelview matrix
    given p, a 3d point to project:

    Compute M y P

    :param camera: The camera for which we want the projection
    :param render: The render settings associated to the scene.
    :return: Camera and projection matrices
    """
    if camera.type != "CAMERA":
        raise Exception("Object {} is not a camera.".format(camera.name))
    # Get the two components to calculate M
    modelview_matrix = camera.matrix_world.inverted()
    projection_matrix = camera.calc_matrix_camera(
        bpy.data.scenes["Scene"].view_layers["ViewLayer"].depsgraph,
        x=render.resolution_x,
        y=render.resolution_y,
        scale_x=render.pixel_aspect_x,
        scale_y=render.pixel_aspect_y,
    )
    return projection_matrix, modelview_matrix


def save_camera_mat(i):
    camera = bpy.data.objects["Camera"]

    # Update scene to ensure up to date matrices
    bpy.context.view_layer.update()

    proj_mat, modelview_mat = get_camera_matrices(camera)

    if not os.path.isdir(BASE_PATH + "/render" + str(i)):
        os.mkdir(BASE_PATH + "/render" + str(i))

    with open(BASE_PATH + "/render" + str(i) + "/camera_mats.bin", "wb") as outfile:
        pickle.dump((np.array(proj_mat), np.array(modelview_mat)), outfile)


for i in range(NUM_RENDERS):
    # Generate Scene

    ## Move camera to known position

    print("{}th iteration".format(i + 1))
    print("Adding and moving objects")

    camera = bpy.data.objects["Camera"]

    camera.location = (
        random.uniform(7, 10),
        random.uniform(-15, -25),
        random.uniform(5, 15),
    )
    camera.rotation_euler = (
        radians(random.uniform(70, 80)),
        radians(random.uniform(-5, 5)),
        radians(random.uniform(-10, 10)),
    )
    save_camera_mat(i)

    ## Create Background
    bpy.ops.mesh.primitive_plane_add(
        size=20,
        location=(0, random.uniform(10, 25), 0),
        rotation=(radians(90), radians(0), radians(0)),
    )
    plane = bpy.context.active_object
    plane.scale = (4, 4, 1)
    plane.name = "Background"

    ### Load image into a material
    background_name = random.choice(["white_background", "black_background"])
    plane.data.materials.append(
        bpy.data.materials.get(background_name)
    )  # A set of images should exist, TODO select this randomly from set

    leaf = bpy.data.objects["Leaf"]

    leaf.pass_index = LEAF_INDEX
    leaf.location = (random.uniform(2, 18), random.uniform(2, 18), random.uniform(1, 3))
    leaf.rotation_euler = (
        radians(random.uniform(0, 180)),
        radians(random.uniform(-45, 45)),
        radians(random.uniform(-30, 30)),
    )
    leaf.scale = (
        random.uniform(4, 8),
        random.uniform(4, 8),
        1,
    )

    bpy.ops.object.modifier_add(type="FLUID")
    water_domain.modifiers["Fluid"].fluid_type = "EFFECTOR"
    water_domain.modifiers["Fluid"].effector_settings.use_plane_init = True

    ## Move Light
    light = bpy.data.objects["Light"]

    light.location = (
        random.uniform(5, 20),
        random.uniform(-35, -25),
        random.uniform(10, 20),
    )
    light.rotation_euler = (radians(15), radians(-65.7), radians(457))

    light.data.type = "SUN"
    light.data.energy = 2

    ## Simulate Smoke & Water

    ### Water
    bpy.ops.mesh.primitive_cube_add(
        location=(15, 15, 10), scale=(15, 15, 10)
    )  # Location & scale should match so that the domain origin is in the World's origin
    water_domain = bpy.context.active_object
    water_domain.name = "Water Domain"
    water_domain.pass_index = WATER_INDEX

    bpy.ops.object.modifier_add(type="FLUID")
    water_domain.modifiers["Fluid"].fluid_type = "DOMAIN"
    water_domain.modifiers["Fluid"].domain_settings.domain_type = "LIQUID"
    water_domain.modifiers["Fluid"].domain_settings.resolution_max = 128
    water_domain.modifiers["Fluid"].domain_settings.use_mesh = True
    water_domain.modifiers["Fluid"].domain_settings.use_speed_vectors = True
    water_domain.modifiers["Fluid"].domain_settings.cache_type = "ALL"
    water_domain.modifiers["Fluid"].domain_settings.cache_data_format = "UNI"
    water_domain.modifiers["Fluid"].domain_settings.cache_frame_start = SIMULATION_START
    water_domain.modifiers["Fluid"].domain_settings.cache_frame_end = SIMULATION_END

    water_domain.data.materials.append(bpy.data.materials.get("Water"))

    # The water material should already exist
    # TODO We should create the domain only once

    #### Water base
    bpy.ops.mesh.primitive_cube_add(
        location=(15, 15, 0.45), scale=(15, 15, random.uniform(0.05, 0.45))
    )  # Location & scale should match so that the domain origin is in the World's origin
    water_base = bpy.context.active_object
    water_base.name = "Water Base"

    bpy.ops.object.modifier_add(type="FLUID")
    water_base.modifiers["Fluid"].fluid_type = "FLOW"
    water_base.modifiers["Fluid"].flow_settings.flow_type = "LIQUID"
    water_base.hide_render = True

    #### Up to N water droplets
    water_droplets = []
    water_droplets = []
    for j in range(random.randint(2, MAX_DROPLETS)):
        droplet_size = random.uniform(0.25, 1.25)
        bpy.ops.mesh.primitive_uv_sphere_add(
            location=(
                random.uniform(2, 18),
                random.uniform(2, 18),
                random.uniform(10, 10),
            ),
            scale=(droplet_size, droplet_size, droplet_size),
        )
        water_droplet = bpy.context.active_object
        water_droplet.name = "Water Drop" + str(j)

        bpy.ops.object.modifier_add(type="FLUID")
        water_droplet.modifiers["Fluid"].fluid_type = "FLOW"
        water_droplet.modifiers["Fluid"].flow_settings.flow_type = "LIQUID"
        water_droplet.modifiers["Fluid"].flow_settings.use_initial_velocity = True
        water_droplet.modifiers["Fluid"].flow_settings.velocity_coord[
            0
        ] = random.uniform(-2.5, 2.5)
        water_droplet.modifiers["Fluid"].flow_settings.velocity_coord[
            1
        ] = random.uniform(-2.5, 2.5)

        water_droplet.hide_render = True

        water_droplets.append(water_droplet)

    print("Baking water simulation")
    water_domain.select_set(True)
    bpy.context.view_layer.objects.active = water_domain
    bpy.ops.fluid.bake_all()
    print("Baking done")

    ### Smoke
    bpy.ops.mesh.primitive_cube_add(
        location=(10, 10, 10), scale=(10, 10, 10)
    )  # Location & scale should match so that the domain origin is in the World's origin
    smoke_domain = bpy.context.active_object
    smoke_domain.name = "Smoke Domain"
    smoke_domain.pass_index = SMOKE_INDEX

    bpy.ops.object.modifier_add(type="FLUID")
    smoke_domain.modifiers["Fluid"].fluid_type = "DOMAIN"
    smoke_domain.modifiers["Fluid"].domain_settings.domain_type = "GAS"
    smoke_domain.modifiers["Fluid"].domain_settings.resolution_max = 32
    smoke_domain.modifiers["Fluid"].domain_settings.timesteps_max = 4
    smoke_domain.modifiers["Fluid"].domain_settings.cache_directory = (
        BASE_PATH + "/render" + str(i) + "/smoke_simulation"
    )
    smoke_domain.modifiers["Fluid"].domain_settings.cache_frame_start = SIMULATION_START
    smoke_domain.modifiers["Fluid"].domain_settings.cache_frame_end = SIMULATION_END
    smoke_domain.modifiers["Fluid"].domain_settings.cache_type = "ALL"

    smoke_domain.data.materials.append(bpy.data.materials.get("Smoke"))
    # The smoke material should already exist
    # TODO We should create the domain only once

    # Add up to N spheres

    bpy.ops.mesh.primitive_uv_sphere_add(
        location=(random.uniform(2, 18), random.uniform(2, 18), random.uniform(1, 3)),
        scale=(
            random.uniform(0.5, 1.5),
            random.uniform(0.5, 1.5),
            random.uniform(0.5, 1.5),
        ),
    )
    smoke_droplet = bpy.context.active_object
    smoke_droplet.name = "Smoke Drop"
    smoke_droplet.hide_render = True

    bpy.ops.object.modifier_add(type="FLUID")
    smoke_droplet.modifiers["Fluid"].fluid_type = "FLOW"
    smoke_droplet.modifiers["Fluid"].flow_settings.flow_type = "SMOKE"
    smoke_droplet.modifiers["Fluid"].flow_settings.velocity_coord[0] = random.uniform(
        -10, -10
    )
    smoke_droplet.modifiers["Fluid"].flow_settings.velocity_coord[1] = random.uniform(
        -10, 10
    )
    smoke_droplet.modifiers["Fluid"].flow_settings.velocity_coord[2] = random.uniform(
        -5, 0.5
    )

    print("Baking smoke simulation")
    smoke_domain.select_set(True)
    bpy.context.view_layer.objects.active = smoke_domain
    bpy.ops.fluid.bake_all()
    print("Baking done")

    # Render configuration, including compositing should be made before hand, only destination directories are modified in this stage
    bpy.data.scenes["Scene"].render.engine = "CYCLES"
    bpy.data.scenes["Scene"].render.resolution_x = 640
    bpy.data.scenes["Scene"].render.resolution_y = 480

    bpy.data.scenes["Scene"].frame_start = SIMULATION_START
    bpy.data.scenes["Scene"].frame_end = SIMULATION_END

    bpy.data.scenes["Scene"].node_tree.nodes["File Output"].base_path = (
        BASE_PATH + "/render" + str(i) + "/original/"
    )

    # Render 1st Pass
    ## This will create the actual training input image

    print("Running 1st render")
    bpy.data.scenes["Scene"].frame_current = 1
    if RENDER:
        bpy.ops.render.render(animation=True)
    print("1st render done")

    # Render 2nd Pass
    ## Reimport smoke data into a mesh

    # water_domain.hide_render = True
    smoke_domain.hide_render = True

    bpy.ops.object.volume_import(
        filepath=BASE_PATH
        + "/render"
        + str(i)
        + "/smoke_simulation/data/fluid_data_0001.vdb"
    )
    smoke_fluid_volume = bpy.data.volumes[-1]
    smoke_fluid_volume.name = "imported_fluid_volume" + str(i)

    smoke_fluid_data = bpy.context.active_object
    smoke_fluid_data.name = "imported_fluid"
    smoke_fluid_data.data = smoke_fluid_volume
    smoke_fluid_data.hide_render = True

    smoke_fluid_volume.is_sequence = True
    smoke_fluid_volume.frame_duration = SIMULATION_END
    smoke_fluid_volume.frame_start = SIMULATION_START

    bpy.ops.mesh.primitive_plane_add(location=(-10, -10, -10))
    smoke_mesh = bpy.context.active_object
    smoke_mesh.name = "Smoke Mesh"
    smoke_mesh.pass_index = SMOKE_INDEX

    bpy.ops.object.modifier_add(type="VOLUME_TO_MESH")
    smoke_mesh.modifiers["Volume to Mesh"].object = smoke_fluid_data
    smoke_mesh.modifiers["Volume to Mesh"].threshold = DENSITY_THRESHOLD

    bpy.data.scenes["Scene"].node_tree.nodes["File Output"].base_path = (
        BASE_PATH + "/render" + str(i) + "/reimported_image"
    )

    print("Running 2nd render")
    bpy.data.scenes["Scene"].frame_current = 1
    if RENDER:
        bpy.ops.render.render(animation=True)
    print("2nd render done")

    plane.select_set(True)
    bpy.ops.object.delete()

    water_domain.select_set(True)
    bpy.ops.object.delete()
    water_base.select_set(True)
    bpy.ops.object.delete()
    for water_droplet in water_droplets:
        water_droplet.select_set(True)
        bpy.ops.object.delete()

    smoke_domain.select_set(True)
    bpy.ops.object.delete()
    smoke_droplet.select_set(True)
    bpy.ops.object.delete()
    smoke_fluid_data.select_set(True)
    bpy.ops.object.delete()
    # smoke_mesh.select_set(True)
    # bpy.ops.object.delete()

    # Delete all volumes
    while len(bpy.data.volumes) > 0:
        bpy.data.volumes.remove(bpy.data.volumes[-1])
