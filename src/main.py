#!/usr/bin/env python

import argparse
import csv
import glob
import math
import os
import pickle

from dataclasses import dataclass
from PIL import Image
import Imath
import numpy as np
import OpenEXR
import pyopenvdb as vdb

import flowiz
import matplotlib.pyplot as plt

from delaunay import get_mesh
from opengl import draw_mesh

get_blender_mat = """
https://blender.stackexchange.com/questions/16472/how-can-i-get-the-cameras-projection-matrix
"""

SMOKE_LABEL = 2
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480

PIXEL_TYPE = Imath.PixelType(Imath.PixelType.FLOAT)
DEPTH_TOLERANCE = 0.1
DENSITY_TOLERANCE = 0.1


@dataclass
class OptFlowPoint2D:
    x: float = 0
    y: float = 0
    z: float = 0
    u: float = 0
    v: float = 0


@dataclass
class Triangle:
    first_vertex: int = 0
    second_vertex: int = 0
    third_vertex: int = 0


def create_opt_flow_label(
    depth_file_path: str,
    fluid_file_path: str,
    proj_mat: np.array,
    modelview_mat: np.array,
):
    depth_image = OpenEXR.InputFile(depth_file_path)
    depth_image = depth_image.channel("R", PIXEL_TYPE)
    depth_label = np.frombuffer(depth_image, dtype=np.float32)
    depth_label.shape = (IMAGE_HEIGHT, IMAGE_WIDTH)

    grids, meta = vdb.readAll(fluid_file_path)

    xy = {"x": [], "y": []}

    uv = {
        "u": [],
        "v": [],
    }

    opt_flow_points = []

    velocity_grid = grids[[grid.name for grid in grids].index("velocity")]
    density_grid = grids[[grid.name for grid in grids].index("density")]
    velocity_metadata = velocity_grid.metadata

    voxel_size = velocity_metadata["file_voxel_size"]

    for velocity_item, density_item in zip(
        velocity_grid.citerOnValues(), density_grid.citerOnValues()
    ):
        (x, y, z) = (velocity_item.min[0], velocity_item.min[1], velocity_item.min[2])
        (u, v, w) = (
            velocity_item.value[0],
            velocity_item.value[1],
            velocity_item.value[2],
        )
        x *= voxel_size
        y *= voxel_size
        z *= voxel_size
        u *= voxel_size
        v *= voxel_size
        w *= voxel_size

        volume_vector = np.array([x, y, z, 1])

        p = OptFlowPoint2D()

        vector_from_cam = modelview_mat @ volume_vector
        p.z = -1 * vector_from_cam[2]
        # depth["z"].append(-1 * vector_from_cam[2])

        plane_vector = proj_mat @ modelview_mat @ volume_vector
        plane_vector /= plane_vector[-1]
        plane_vector = plane_vector[0:2]
        plane_vector += [1, -1]
        plane_vector *= [(IMAGE_WIDTH - 1) / 2, (IMAGE_HEIGHT - 1) / (-2)]

        p.x = plane_vector[0]
        p.y = plane_vector[1]

        if p.x < 0 or p.x > IMAGE_WIDTH or p.y < 0 or p.y > IMAGE_HEIGHT:
            continue

        volume_vector_end = volume_vector + np.array([(u), (v), (w), 0])

        plane_vector_end = proj_mat @ modelview_mat @ volume_vector_end
        plane_vector_end /= plane_vector_end[-1]
        plane_vector_end = plane_vector_end[0:2]
        plane_vector_end += [1, -1]
        plane_vector_end *= [(IMAGE_WIDTH - 1) / 2, (IMAGE_HEIGHT - 1) / (-2)]

        p.u, p.v = plane_vector_end - plane_vector

        depth_pixel = depth_label[math.floor(p.y), math.floor(p.x)]
        if (
            (depth_pixel > ((1 - DEPTH_TOLERANCE) * p.z))
            and (depth_pixel < ((1 + DEPTH_TOLERANCE) * p.z))
            and density_item.value > DENSITY_TOLERANCE
        ):

            xy["x"].append(plane_vector[0])
            xy["y"].append(plane_vector[1])
            uv["u"].append((plane_vector_end - plane_vector)[0])
            uv["v"].append((plane_vector_end - plane_vector)[1])

            opt_flow_points.append(p)

    delaunay_points = []
    for point in opt_flow_points:
        delaunay_points.append([point.x, point.y])

    if not delaunay_points or len(delaunay_points) < 5:
        return

    mesh = get_mesh(delaunay_points)

    triangles_vertices = []
    for simplex in mesh.simplices:
        triangle = Triangle(simplex[0], simplex[1], simplex[2])

        triangles_vertices.append(triangle)

    u_label, v_label = draw_mesh(
        IMAGE_WIDTH, IMAGE_HEIGHT, opt_flow_points, triangles_vertices
    )

    uv_label = np.stack([u_label, v_label], axis=2)

    return uv_label


def writeFlo(in_img: np.array, flo_file_name: str):
    height = in_img.shape[0]
    width = in_img.shape[1]

    R = (in_img[:, :, 0]).astype(np.float32)
    G = (in_img[:, :, 1]).astype(np.float32)

    flo_img = np.zeros((height, width, 2), np.float32)  # 2 channels, u & v
    flo_img[:, :, 0] = -np.array(R).reshape(in_img.shape[0], -1)
    flo_img[:, :, 1] = np.array(G).reshape(in_img.shape[0], -1)

    with open(flo_file_name, "wb") as obj_out:
        np.array([80, 73, 69, 72], np.uint8).tofile(
            obj_out
        )  # Save the Header to the .flo file
        np.array([width, height], np.int32).tofile(obj_out)
        np.array(flo_img, np.float32).tofile(obj_out)


def get_final_flow_label(orig_flow_file_path: str, uv_label: str, segm_file_path: str):
    original_flow = OpenEXR.InputFile(orig_flow_file_path)

    original_flow_u = original_flow.channel("R", PIXEL_TYPE)
    original_flow_u = np.frombuffer(original_flow_u, dtype=np.float32)
    original_flow_u.shape = (IMAGE_HEIGHT, IMAGE_WIDTH)
    original_flow_v = original_flow.channel("G", PIXEL_TYPE)
    original_flow_v = np.frombuffer(original_flow_v, dtype=np.float32)
    original_flow_v.shape = (IMAGE_HEIGHT, IMAGE_WIDTH)

    seg_label = np.array(Image.open(segm_file_path))

    smoke_pixels_mask = seg_label == SMOKE_LABEL
    non_smoke_pixels_mask = seg_label != SMOKE_LABEL

    original_flow_u = non_smoke_pixels_mask * original_flow_u
    original_flow_v = non_smoke_pixels_mask * original_flow_v
    final_non_smoke_label = np.stack([original_flow_u, original_flow_v], axis=2)

    if uv_label is not None:
        final_u_label = uv_label[:, :, 0] * smoke_pixels_mask
        final_v_label = uv_label[:, :, 1] * smoke_pixels_mask

        final_smoke_label = np.stack([final_u_label, final_v_label], axis=2)
        final_flow_label = final_non_smoke_label + final_smoke_label

    else:
        final_flow_label = final_non_smoke_label

    return final_flow_label


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--base_path",
        type=str,
        required=True,
        help="Base path, should include, reimported_image, original and smoke_simulation folders",
    )
    parser.add_argument(
        "--orig_folder",
        type=str,
        help="Name for the folder containing original Vectoru4.exr files that don't have the smoke data",
        default="original",
    )
    parser.add_argument(
        "--reimported_folder",
        type=str,
        help="Name for the folder containing reimported Segmentationu4.png and Depthu4.exr files which actually have the smoke data",
        default="reimported_image",
    )
    parser.add_argument(
        "--smoke_path",
        type=str,
        help="Name for the folder fluid_data_u4.vbd files corresponding to the vdb files",
        default="smoke_simulation/data/",
    )
    parser.add_argument(
        "--output_path", type=str, help="Name for output_Folder", default="final_flow/"
    )
    args = parser.parse_args()

    base_path = os.path.abspath(args.base_path)
    orig_image_files = glob.glob(
        base_path + "/" + args.orig_folder + "/Image[0-9]*.png"
    )
    segmentation_files = glob.glob(
        base_path + "/" + args.reimported_folder + "/Segmentation[0-9]*.png"
    )
    depth_files = glob.glob(
        base_path + "/" + args.reimported_folder + "/Depth[0-9]*.exr"
    )
    fluid_data_files = glob.glob(
        base_path + "/" + args.smoke_path + "/fluid_data_[0-9]*.vdb"
    )
    original_flow_files = glob.glob(
        base_path + "/" + args.orig_folder + "/Vector[0-9]*.exr"
    )

    depth_files.sort()
    fluid_data_files.sort()
    original_flow_files.sort()
    segmentation_files.sort()
    orig_image_files.sort()

    with open(base_path + "/camera_mats.bin", "rb") as mat_file:
        proj_mat, modelview_mat = pickle.load(mat_file)

    print(proj_mat, modelview_mat)

    if not os.path.isdir("{}/{}".format(base_path, args.output_path)):
        os.mkdir("{}/{}".format(base_path, args.output_path))

    csv_fields = ["cnt", "image", "flow_label", "segm_label"]
    rows = []

    for (
        i,
        (
            depth_file_path,
            fluid_file_path,
            orig_flow_file_path,
            segm_file_path,
            orig_image_path,
        ),
    ) in enumerate(
        zip(
            depth_files,
            fluid_data_files,
            original_flow_files,
            segmentation_files,
            orig_image_files,
        )
    ):
        uv_label = create_opt_flow_label(
            depth_file_path, fluid_file_path, proj_mat, modelview_mat
        )

        final_flow_label = get_final_flow_label(
            orig_flow_file_path, uv_label, segm_file_path
        )

        final_vector_path = "{base_path}/{output_path}Vector{cnt:04d}.flo".format(
            base_path=base_path, output_path=args.output_path, cnt=i
        )

        writeFlo(final_flow_label, final_vector_path)

        # Create a symlink for the other labels
        final_image_path = "{base_path}/{output_path}/Image{cnt:04d}.png".format(
            base_path=base_path, output_path=args.output_path, cnt=i
        )
        if os.path.isfile(final_image_path):
            os.remove(final_image_path)
        os.symlink(orig_image_path, final_image_path)

        final_segm_path = "{base_path}/{output_path}/Segmentation{cnt:04d}.png".format(
            base_path=base_path, output_path=args.output_path, cnt=i
        )
        if os.path.isfile(final_segm_path):
            os.remove(final_segm_path)
        os.symlink(segm_file_path, final_segm_path)

        rows.append(
            [
                i,
                "Image{:04d}.png".format(i),
                "Vector{:04d}.exr".format(i),
                "Segmentation{:04d}.png".format(i),
            ]
        )

    with open(base_path + "/" + args.output_path + "dataset.csv", "w") as csvfile:
        # creating a csv writer object
        csv_writer = csv.writer(csvfile)

        # writing the fields
        csv_writer.writerow(csv_fields)

        # writing the data rows
        csv_writer.writerows(rows)
