# Taken from
# https://gist.githubusercontent.com/nmullane/508c857b20d8547e5e8cbcc21f9834cf/raw/cecdc18abc07e2707115bec6ba016be4e4faac2e/delaunay.py

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
from scipy.spatial import ConvexHull
from math import sqrt, ceil


def get_mesh(points):
    if len(points) == 0:
        return
    npPoints = np.array(points)

    triangulation = Delaunay(npPoints)

    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # ax.triplot(npPoints[:, 0], npPoints[:, 1], triangulation.simplices)
    # plt.xlim((0, 640))
    # plt.ylim((0, 480))

    return triangulation
