# Modified from www.labri.fr/perso/nrougier/python+opengl

from operator import attrgetter

import numpy as np
from numpy.core.fromnumeric import transpose
from glumpy import app, gl, glm, gloo

import matplotlib.pyplot as plt


def normalize(data, old_min, old_max, new_min=0, new_max=1):
    m = (new_max - new_min) / (old_max - old_min)
    b = new_min - m * old_min
    return data * m + b


def draw_mesh(width, height, opt_flow_points, triangles_vertices):
    vertex = """
    #version 440
    precision highp float;
    in vec3 color;         // Vertex color
    in vec2 position;      // Vertex position
    out vec4   v_color;       // Interpolated fragment color (out)
    void main()
    {
        v_color = vec4(color, 1.0);
        gl_Position = vec4(position, 0.0, 1.0);
    }
    """

    fragment = """
    #version 440
    precision highp float;
    in vec4 v_color; // Interpolated fragment color (in)
    out vec4 color;  // this is the output color, going to GL_COLOR_ATTACHEMENT0
    void main()
    {
        color = v_color;
    }
    """

    # Global maximun and minium for both dimensions
    max_color = max(
        max(opt_flow_points, key=attrgetter("u")).u,
        max(opt_flow_points, key=attrgetter("u")).v,
    )

    min_color = min(
        min(opt_flow_points, key=attrgetter("u")).u,
        min(opt_flow_points, key=attrgetter("u")).v,
    )

    # We work on vertex triplets, each has 2 positions and 2 directions, the
    # third direction is always left as 0
    V = np.zeros(
        len(opt_flow_points), [("position", np.float32, 2), ("color", np.float32, 3)]
    )
    # We need 3 times as many triplets as triangles
    I = np.zeros(3 * len(triangles_vertices), dtype=np.uint32)

    for i, point in enumerate(opt_flow_points):
        x = normalize(point.x, 0, width, new_min=-1)
        y = normalize(point.y, 0, height, new_min=-1)

        u = normalize(point.u, min_color, max_color)
        v = normalize(point.v, min_color, max_color)

        V["position"][i] = np.array([x, y], dtype=np.float32)

        V["color"][i] = np.array([u, v, 0], dtype=np.float32)

    for i, triangle in enumerate(triangles_vertices):
        I[i * 3 + 0] = triangle.first_vertex
        I[i * 3 + 1] = triangle.second_vertex
        I[i * 3 + 2] = triangle.third_vertex

    V = V.view(gloo.VertexBuffer)
    I = I.view(gloo.IndexBuffer)

    # Initialize a float 2d texture. The "base value" correspondes to a normalized 0
    # since we want to operate in the range of our movement
    texture = np.zeros((height, width, 4), np.float32)
    opengl_zero = normalize(0, min_color, max_color)
    texture += opengl_zero
    texture = texture.view(gloo.TextureFloat2D)
    framebuffer = gloo.FrameBuffer(color=[texture])
    optical_flow = gloo.Program(vertex, fragment, version=430)
    optical_flow.bind(V)

    window = app.Window(
        width=width,
        height=height,
        color=(opengl_zero, opengl_zero, opengl_zero, opengl_zero),
    )

    r_texture = 0
    g_texture = 0

    @window.event
    def on_draw(dt):
        nonlocal r_texture
        nonlocal g_texture

        window.clear()

        framebuffer.activate()
        optical_flow.draw(gl.GL_TRIANGLES, I)
        # optical_flow.draw(gl.GL_TRIANGLE_STRIP)
        # The texture can be read as np.ndarray.
        r_texture = framebuffer.color[0].get()[:, :, 0]
        g_texture = framebuffer.color[0].get()[:, :, 1]
        framebuffer.deactivate()

    app.run(framecount=0)

    r_texture = normalize(r_texture, 0, 1, min_color, max_color)
    g_texture = normalize(g_texture, 0, 1, min_color, max_color)

    window.close()
    window.destroy()

    return r_texture, g_texture
